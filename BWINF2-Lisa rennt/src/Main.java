import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.Graphics2D;
import javax.swing.JFrame;

public class Main {
	public static void main(String[] args) {
		JFrame frame = new JFrame("My Drawing");
		Street s = new Street(300,300);
        frame.getContentPane().add(s);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        s.initialize();
        s.aStarTest();
        s.scale(2.0);
        /*BufferedImage b = new BufferedImage(400, 400, BufferedImage.TYPE_INT_ARGB);
        s.paint(b.getGraphics());
        int[] pixels = ((DataBufferInt) b.getRaster().getDataBuffer()).getData();
        for(int i:pixels) System.out.println(i);*/

       
        
        /*AStar a = new AStar();		
		Node[][] board = s.board;
		
        List<Node> path = a.findPath(start, goal, board);
        s.setPath(path);*/
	}
	
}
