
public class Node implements Comparable<Node>{
	// x und y -> Koordinatensystem koordinaten
	int x, y;
	//cx und cy -> Canvas Koordinaten
	int cx,cy;
	
	boolean blocked;
	
	int f,g;
	Node parent;

	public Node(int x, int y, int cx, int cy, boolean blocked) {
		this.x = x;
		this.y = y;
		this.cx = cx;
		this.cy = cy;
		this.blocked = blocked;
		f = 0;
		g = 0;
	}
	
	public boolean isBlocked() {
		return blocked;
	}
	public int getRealX() {
		return x;
	}
	public int getRealY() {
		return y;
	}
	
	public int getCanvasX() {
		return cx;
	}
	public int getCanvasY() {
		return cy;
	}

	//F�r PriorityQueue, f = gesch�tzte Distanz zum Ziel
	@Override
	public int compareTo(Node otherNode) {
		if(this.f == otherNode.f) return 0;
		else return this.f>otherNode.f?1:-1;
	}
	

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
}
