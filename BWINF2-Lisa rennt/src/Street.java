import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Street extends Canvas {
	
	private static final long serialVersionUID = 2307805329367025108L;
	
	Node[][] board;
	List<Node> path;
	final int height, width;
	
	double SCALE_FACTOR;
	private static final int FIELD_SIZE = 1;
	private static final Color PATH_COLOR = Color.RED;
	private static final Color STANDARD_COLOR = Color.BLACK;
	private static final Color GOAL_COLOR = Color.GREEN;
	private static final Color START_COLOR = Color.YELLOW;
	
	private final int STANDARD_WINDOW_HEIGHT;
	private final int STANDARD_WINDOW_WIDTH;
	
	public Street(int width, int height) {
		this.width = width;
		this.height = height;
		STANDARD_WINDOW_HEIGHT = height*FIELD_SIZE;
		STANDARD_WINDOW_WIDTH = width*FIELD_SIZE;
		
		SCALE_FACTOR = 1;
		
		
		this.path = null;
		
		board = new Node[height][width];
		this.setSize(STANDARD_WINDOW_WIDTH,STANDARD_WINDOW_HEIGHT);
		
	}
	
	public void initialize() {
		initNodes();
		drawField();
		drawPath();
		//repaint();
	}
	
	private void initNodes() {
		for(int y = 0; y<height;y++) {
			for(int x = 0; x<width;x++) {
				Node n = new Node(x,y,x*FIELD_SIZE,STANDARD_WINDOW_HEIGHT-(y*FIELD_SIZE),false);
				if(Math.random()>0.7) n.setBlocked(true);
				board[y][x] = n;
			}
		}
	}
	
	private void pointAt(int x, int y, Color c) {
		Graphics g = this.getGraphics();
		g.setColor(c);
		g.fillRect((int)(x*FIELD_SIZE*SCALE_FACTOR), (int)(STANDARD_WINDOW_HEIGHT-(y*FIELD_SIZE*SCALE_FACTOR)), FIELD_SIZE, FIELD_SIZE);
	}	

	public void paint(Graphics g) {
		drawField();
		drawPath();
	}
	
	public void drawField() {
		for(int y= 0;y<height;y++) {
			for(int x = 0;x<width;x++) {
				Node c = board[y][x];
				if(c.isBlocked()) pointAt(c.getRealX(),c.getRealY(),Color.BLACK);
			}
		};
	}
	
	private void drawPath() {
		if(path==null) return;
		for(int i = 0; i<path.size();i++) {
			Node n = path.get(i);
			Color point = i==0?START_COLOR:i==path.size()-1?GOAL_COLOR:PATH_COLOR;
			pointAt(n.getRealX(),n.getRealY(),point);		
		}
	}
	
	private void refreshPath(List<Node> path) {
		this.path = path;
		drawPath();
	}
	
	public void scale(Double d) {
		SCALE_FACTOR = d;
		((Graphics2D)(getGraphics())).scale(d, d);
	}
	
	public void aStarTest() {
		Object t = ThreadLocalRandom.current();
		int x1 = ((ThreadLocalRandom) t).nextInt(0, width/2 + 1);
		int x2 = ((ThreadLocalRandom) t).nextInt(width/2, width);
		int y1 = ((ThreadLocalRandom) t).nextInt(0, height/2 + 1);
		int y2 = ((ThreadLocalRandom) t).nextInt(height/2, height);
		
		Node start = board[y1][x1];
		Node goal = board[y2][x2];
		
		System.out.println("Start: ("+start.getRealX()+"|"+start.getRealY()+")");
		System.out.println("Ziel: ("+goal.getRealX()+"|"+goal.getRealY()+")");
		
		start.setBlocked(false);
		goal.setBlocked(false);
			
		AStar a = new AStar();		
		
        refreshPath(a.findPath(start, goal, board));
	}
}
