import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import com.google.common.graph.*;
public class AStar {
	
	Queue<Node> open;
	Set<Node> closed;
	
	public AStar() {
		 open = new PriorityQueue<Node>();
		 closed = new HashSet<Node>();
	}
	
	public List<Node> findPath(Node start, Node goal, Node[][] board){
		Graph<Node> g = generateGraph(board);
		Node n = generatePath(goal, start, g);
		if(n==null) {
			System.out.println("Kein Weg gefunden :shrug:");
			return new ArrayList<Node>();
		}
		else {
			Node current = n;
			ArrayList<Node> path = new ArrayList<Node>();
			while(current!=goal) {
				path.add(current);
				current = current.getParent();
			}
			return path;
		}
		
	}
	
	private MutableGraph<Node> generateGraph(Node[][] board) {
		MutableGraph<Node> graph = GraphBuilder.undirected().allowsSelfLoops(false).build();
		
		int height = board.length;
		int width = board[0].length;
		
		for(Node[] row : board) {
			for(Node n : row) graph.addNode(n);
		}
		
		for(int y = 0; y<height;y++) {	
			for(int x = 0; x<width;x++) {
				Node actNode = board[y][x];
				if(actNode.isBlocked()) continue;
				else {
					
					//Edge zu rechtem Feld
					if(x<width-1 && !board[y][x+1].isBlocked()) graph.putEdge(actNode,board[y][x+1]);
					//Edge zu unterem Feld
					if(y<height-1 && !board[y+1][x].isBlocked()) graph.putEdge(actNode,board[y+1][x]);
					//OPTIONAL: Edge zu rechts unten
					if(y<height-1 && x<width-1 && !board[y+1][x+1].isBlocked()) {
						if(!board[y+1][x].isBlocked() && !board[y][x+1].isBlocked()) graph.putEdge(actNode, board[y+1][x+1]);
					}
					//OPTIONAL: Edge zu links unten
					if(y<height-1 && x>0 && !board[y+1][x-1].isBlocked()) {
						if(!board[y+1][x].isBlocked() && !board[y][x-1].isBlocked()) graph.putEdge(actNode, board[y+1][x-1]);
					}
				}
			}
		}
		return graph;
	}
	
	private Node generatePath(Node start, Node goal, Graph<Node> graph) {		
		open.add(start);
		do {
			Node current = open.poll();
			
			if(current == goal) {
				return current;
			}
			closed.add(current);
			for(Node successor : graph.adjacentNodes(current)) {
				
				if(closed.contains(successor)) continue;
				int new_g = current.g+1;
				if(open.contains(successor) && new_g >= successor.g) continue;
				successor.setParent(current);
				successor.g = new_g;
				
				int new_f = new_g + heuristic(successor,goal);
				successor.f = new_f; 
				open.add(successor);
			}	
		} while(!open.isEmpty());
		return null;
	}
	
	public int heuristic(Node n1, Node n2) {
		return Math.abs(n1.x-n2.x) + Math.abs(n1.y-n2.y);
	}
}
